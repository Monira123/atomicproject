-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 23, 2016 at 04:30 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gender`
--

-- --------------------------------------------------------

--
-- Table structure for table `gender_select`
--

CREATE TABLE IF NOT EXISTS `gender_select` (
`id` int(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender_select`
--

INSERT INTO `gender_select` (`id`, `title`, `gender`) VALUES
(1, 'tryytr', 'Male'),
(2, 'tryytr', 'Male'),
(3, 'tryytr', 'Male'),
(4, 'tryytr', 'Male'),
(5, '', ''),
(6, 'tryytr', 'Male'),
(7, 'tryytr', 'Male'),
(8, 'tryytr', 'Male'),
(9, 'fdggfd', 'Male'),
(10, 'fdggfd', 'Male');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `gender_select`
--
ALTER TABLE `gender_select`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `gender_select`
--
ALTER TABLE `gender_select`
MODIFY `id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
