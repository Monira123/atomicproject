<?php

namespace App\BITM\SEIP106339;

use \App\BITM\SEIP106339\Message;
use App\BITM\SEIP106339\Utility;

class Email {

    public $id = "";
    public $title = "";
    public $email = "";

    public function index() {
        $_books = array();
        //echo "hi";
        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
        //echo $conn;
        $lnk = mysql_select_db("newsletter") or die("Cannot select database.");
        $query = "SELECT * FROM `email`";
        $result = mysql_query($query);

        while ($row = mysql_fetch_assoc($result)) {
            $_books[] = $row;
        }
        return $_books;
    }

    public function show($id = NULL) {
        if (is_null($id)) {
            return;
        }
        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
//echo $conn;
        $lnk = mysql_select_db("newsletter") or die("Cannot select database.");
        $query = "SELECT * FROM `email` WHERE id=" . $id;
        $b = mysql_query($query);
        $result = mysql_fetch_object($b);
        return $result;
    }

    public function store() {
        //echo $title, $author;
        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
        //echo $conn;
        $lnk = mysql_select_db("newsletter") or die("Cannot select database.");
        $query = "INSERT INTO `newsletter`.`email` ( `title`,`email`) VALUES ( '" . $_REQUEST['title'] . "','" . $_REQUEST['email'] . "')";
        //echo $query;
        if (mysql_query($query)) {
            Message::set('Email data added successfuly');
        } else {
            Message::set('Error,please try again');
        }
        header('location:index.php');
    }

    public function edit($id = NULL) {
        if (is_null($id)) {
            return;
        }
        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
//echo $conn;
        $lnk = mysql_select_db("newsletter") or die("Cannot select database.");
        $query = "SELECT * FROM `email` WHERE id=" . $id;
        $b = mysql_query($query);
        $result = mysql_fetch_object($b);
        return $result;
        // $result= mysql_fetch_object($b);
        // Utility::dd($result);
        //Utility::dd();
    }

    public function update() {
        //echo $title, $author;
        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
//echo $conn;
        $lnk = mysql_select_db("newsletter") or die("Cannot select database.");
        $query = "UPDATE `email` SET `title` = '" . $this->title . "', `email` = '" . $this->email . "' WHERE `email`.`id` = " . $this->id;
//echo $query;
//die();
        if (mysql_query($query)) {
            Message::set('Email data updated successfuly');
        } else {
            Message::set('Error,please try again');
        }
        Utility::redirect("index.php");
    }

    public function delete($id = NULL) {
        if (is_null($id)) {
            return;
        }
        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
//echo $conn;
        $lnk = mysql_select_db("newsletter") or die("Cannot select database.");
        $query = "DELETE FROM `newsletter`.`email` WHERE `email`.`id`=" . $id;
        if ($b = mysql_query($query)) {
            Message::set('Email data deleted successfuly');
        } else {
            Message::set('Error,please try again');
        }
        //$result=  mysql_fetch_object($b);
        //return $result;
        Utility::redirect("index.php");
    }

    public function prepare($data = array()) {
        //Utility::dd($data);
        if (is_array($data) && array_key_exists('title', $data)) {

            $this->title = $data['title'];
            $this->email = $data['email'];

            if (array_key_exists('id', $data) && !empty($data['id'])) {
                $this->id = $data['id'];
            }
        }
        return $this;
    }

}
