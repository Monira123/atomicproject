<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Hobby
 *
 * @author Web App Develop PHP
 */

namespace App\BITM\SEIP106339;

use \App\BITM\SEIP106339\Message;
use App\BITM\SEIP106339\Utility;

class Hobby {

    public $id = "";
    public $title = "";
    public $hobby = "";

    public function index() {
        $_books = array();
        //echo "hi";
        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
        //echo $conn;
        $lnk = mysql_select_db("favourite") or die("Cannot select database.");
        $query = "SELECT * FROM `hobby`";
        $result = mysql_query($query);

        while ($row = mysql_fetch_assoc($result)) {
            $_books[] = $row;
        }
        return $_books;
    }

    public function store() {

        // die();


        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
        //echo $conn;
        $lnk = mysql_select_db("favourite") or die("Cannot select database.");

        $checkBox = implode(',', $_REQUEST['hobby']);
        if (isset($_REQUEST['submit'])) {
            $query = "INSERT INTO `favourite`.`hobby` ( `title`,`hobby`) VALUES ( '" . $_REQUEST['title'] . "','" . $checkBox . "')";
            //echo $query;
            if (mysql_query($query)) {
                Message::set('Hobby data added successfuly');
            } else {
                Message::set('Error,please try again');
            }
            //echo "Complete";
        }
        //die();

        header('location:index.php');
    }

    public function show($id = NULL) {
        if (is_null($id)) {
            return;
        }
        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
//echo $conn;
        $lnk = mysql_select_db("favourite") or die("Cannot select database.");
        $query = "SELECT * FROM `hobby` WHERE id=" . $id;
        $b = mysql_query($query);
        $result = mysql_fetch_object($b);
        return $result;
    }

    public function edit($id = NULL) {
        if (is_null($id)) {
            return;
        }
        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
//echo $conn;
        $lnk = mysql_select_db("favourite") or die("Cannot select database.");

        $query = "SELECT * FROM `hobby` WHERE id=" . $id;
        $b = mysql_query($query);
        $result = mysql_fetch_object($b);
        return $result;
    }

    public function update() {
        //echo $title, $author;
        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
//echo $conn;
        $lnk = mysql_select_db("favourite") or die("Cannot select database.");
        $checkBox = implode(',', $_REQUEST['hobby']);
        $query = "UPDATE `hobby` SET `title` = '" . $this->title . "', `hobby` = '" . $checkBox . "' WHERE `hobby`.`id` = " . $this->id;
//echo $query;
//die();
        if (mysql_query($query)) {
            Message::set('Hobby data updated successfuly');
        } else {
            Message::set('Error,please try again');
        }
        Utility::redirect("index.php");
    }

    public function delete($id = NULL) {
        if (is_null($id)) {
            return;
        }
        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
//echo $conn;
        $lnk = mysql_select_db("favourite") or die("Cannot select database.");
        $query = "DELETE FROM `favourite`.`hobby` WHERE `hobby`.`id`=" . $id;
        if ($b = mysql_query($query)) {
            Message::set('Hobby data deleted successfuly');
        } else {
            Message::set('Error,please try again');
        }
        //$result=  mysql_fetch_object($b);
        //return $result;
        Utility::redirect("index.php");
    }

    public function prepare($data = array()) {
        //Utility::dd($data);
        if (is_array($data) && array_key_exists('title', $data)) {

            $this->title = $data['title'];
            $this->hobby = $data['hobby'];

            if (array_key_exists('id', $data) && !empty($data['id'])) {
                $this->id = $data['id'];
            }
        }
        return $this;
    }

}
