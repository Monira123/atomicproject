<?php


include_once ("../../../"."vendor/autoload.php");
use \App\BITM\SEIP106339\Gender;
use App\BITM\SEIP106339\Utility;

$book= new Gender();
$books=$book->edit($_GET['id']);
//Utility::dd($books);
//var_dump($books);
//die();
?>


<html>
    <head>
        <title>Edit</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap -->
        <link href="../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <form action="update.php" method="post" class="form-inline">
                <div class="col-md-6">
                    <div><br>
                        <input type="hidden" class="form-control" 
                               name="id"
                               value="<?php echo $books->id;?>">
                        <label for="name">Book name:  </label>
                        <input type="text" 
                               name="title"
                               class="form-control" 
                               value="<?php echo $books->title;?>">
                    </div><br>
                    
                    <div>
                        <label for="id">Gender: </label>
                        <label><input name="gender" type="radio" value="Male" <?php if(preg_match('/Male/', $books->gender)){ echo "checked";} ?>>Male</label>
                        <label><input name="gender" type="radio" value="Female"<?php if(preg_match('/Female/', $books->gender)){ echo "checked";} ?>>Female</label>
                         <?php  $books->gender;?>
                                                           
                               <button type="submit" class="btn btn-primary">Update</button>
                    </div></div>



            </form>
            
        </div><div><center><a href="index.php">Go to list</a></center></div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../../resource/bootstrap/css/bootstrap.min.css"></script>
    </body>
</html>

