<?php


include_once ("../../../"."vendor/autoload.php");
use \App\BITM\SEIP106339\Hobby;
use App\BITM\SEIP106339\Utility;

$book= new Hobby();
$books=$book->edit($_GET['id']);
//var_dump($books);
//die();
//Utility::dd($books);
?>

<html>
    <head>
        <title>Create</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap -->
        <link href="../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <form action="update.php" method="post" class="form-inline">
                <div class="col-md-6">
                    <div><br>
                        <label for="id"></label>
                        <input type="hidden" class="form-control" 
                               name="id"
                               id="name" 
                               value="<?php echo $books->id;?>">
                    </div><br>
                    <div><br>
                        <label for="name">Enter name:  </label>
                        <input type="text" class="form-control" 
                               name="title"
                               id="name" 
                               value="<?php echo $books->title;?>">
                    </div><br>

                    <div>
                        <label for="id">Select Hobby/Hobbies: </label><br>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="hobby[]" id="checkboxSuccess" value="Cricket"<?php if(preg_match('/Cricket/', $books->hobby)){ echo "checked";} ?>>
                                Cricket
                            </label><br>
                            <label>
                                <input type="checkbox" name="hobby[]" id="checkboxSuccess" value="Football"<?php if(preg_match('/Football/', $books->hobby)){ echo "checked";} ?>>
                                Football
                            </label><br>
                            <label>
                                <input type="checkbox" name="hobby[]" id="checkboxSuccess" value="Cycling"<?php if(preg_match('/Cycling/', $books->hobby)){ echo "checked";} ?>>
                                Cycling
                            </label><br>
                            <label>
                                <input type="checkbox" name="hobby[]" id="checkboxSuccess" value="Photography"<?php if(preg_match('/Photography/', $books->hobby)){ echo "checked";} ?>>
                                Photography
                            </label><br>
                            <label>
                                <input type="checkbox" name="hobby[]" id="checkboxSuccess" value="Swimming"<?php if(preg_match('/Swimming/', $books->hobby)){ echo "checked";} ?>>
                                Swimming
                            </label><br>
                            <?php  $books->hobby;?>

                            <button type="submit" name="submit" class="btn btn-primary">Update</button>
                        </div></div>



            </form>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../../resource/bootstrap/css/bootstrap.min.css"></script>
    </body>
</html>
